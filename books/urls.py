from django.urls import path

from books.views import index, search_books, show_books

urlpatterns = [
    path("", index),
    path("books/", show_books),
    path("search/", search_books),
]