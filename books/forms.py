from django.forms import ModelForm
from books.models import Book

class BookForm(ModelForm):
    class Meta:
        # this is the model that we want a form for
        model = Book

        # this tells the form to create form fields
        # for all of the fields that Book has
        fields = "__all__"
